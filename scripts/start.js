#!/usr/bin/env node --experimental-worker
const path = require("path");
const Vasco = require("../index.js");
let configPath = path.resolve(process.cwd(), "./vasco.config.js");
let config = require(configPath);
let vasco = new Vasco(config);

vasco.start();

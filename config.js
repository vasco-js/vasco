const convict = require("convict");

const fs = require("fs-extra");

let schema = {
  dev: {
    doc: "Controls global dev mode",
    format: Boolean,
    default: process.env.NODE_ENV !== "production"
  },
  rootDir: {
    doc: "Projects root dir [required]",
    format: function(val) {
      return fs.pathExistsSync(val);
    },
    default: null
  },
  dir: {
    build: {
      doc: "Build dir (relative to root)",
      default: ".vasco"
    },
    algorithms: {
      doc: "Algorithms dir (relative to root)",
      default: "algorithms"
    },
    plugins: {
      doc: "Plugins dir (relative to root)",
      default: "plugins"
    }
  },
  // db: {
  //   address: "",
  //   auth: false
  // },
  tokens: {
    google: {
      doc:
        "Google token used for geocoding and autocompletion etc ... [required]",
      format: String,
      default: null,
      env: "GOOGLE_TOKEN"
    }
  },
  client: {
    disable: {
      format: Boolean,
      default: false
    },
    debug: {
      doc: "Client's debug mode",
      format: Boolean,
      default: false
    }
  },
  server: {
    ssl: {
      enabled: {
        doc: "Use ssl",
        default: false
      }
    },
    host: {
      doc: "Hostname",
      default: "localhost"
    },
    port: {
      doc: "Port",
      default: 9293,
      env: "PORT",
      arg: "port"
    }
  }
};

module.exports = schema;

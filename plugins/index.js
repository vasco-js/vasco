const path = require("path");
const fs = require("fs-extra");

class VascoPluginManager {
  #vasco;
  constructor(vasco) {
    this.#vasco = vasco;

    this.loadFromDir(path.resolve(__dirname, "../core/plugins"));
  }
  loadFromDir(dir) {
    let sources = fs
      .readdirSync(dir)
      .map(name => path.join(dir, name))
      .filter(source => fs.lstatSync(source).isDirectory());
    this.install(...sources);
  }
  install(...plugins) {
    let vasco = this.#vasco;
    let logger = vasco.logger;

    plugins.forEach(function(src) {
      try {
        let plugin = require(src);
        plugin.install(vasco);
      } catch (e) {
        logger.error(e.stack);
        throw `Can't load plugin at path ${src}`;
      }
    });
  }
}

module.exports = VascoPluginManager;

const path = require("path");
const fs = require("fs-extra");
const lowdb = require("lowdb");
const axios = require("axios");

let $axios = axios.create({
  timeout: 2000
});

module.exports = {
  install(vasco) {
    let logger = vasco.logger;

    // db for storing results
    const FileSync = require("lowdb/adapters/FileSync");
    let save_file = path.resolve(vasco.getDir("build"), "places", "saves.json");
    fs.ensureFileSync(save_file);
    const adapter = new FileSync(save_file);
    const db = lowdb(adapter);
    db.defaults({ locations: [] }).write();
    const locations = db.get("locations");

    function saveLocation(loc) {
      let saved = locations.find({ id: loc.id }).value();
      if (!saved) {
        locations.push(loc).write();
      }
    }

    // define functions
    let GOOGLE_TOKEN = vasco.config.get("tokens.google");
    async function autocomplete(input) {
      try {
        let res = await $axios.get(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json",
          {
            params: {
              key: GOOGLE_TOKEN,
              input,
              fields: "place_id,geometry/location,formatted_address",
              type: "(cities)"
            }
          }
        );
        let data = res.data;
        if (data.status == "OK") {
          let candidates = data.predictions.map(
            ({ place_id, description }) => ({
              place_id,
              description
            })
          );
          return candidates;
        } else if (data.status == "ZERO_RESULTS") {
          return [];
        } else {
          throw `request failed with status ${data.status} : ${data.error_message}`;
        }
      } catch (e) {
        throw e;
      }
    }
    async function geocode(place_id) {
      let saved = locations.find({ id: place_id }).value();
      if (saved) {
        return saved;
      } else {
        try {
          let res = await $axios.get(
            "https://maps.googleapis.com/maps/api/place/details/json",
            {
              params: {
                key: GOOGLE_TOKEN,
                place_id,
                fields: "geometry/location,formatted_address"
              }
            }
          );
          let data = res.data;
          if (data.status == "OK") {
            let { lat, lng } = data.result.geometry.location;
            let location = {
              id: place_id,
              location: { lat, lon: lng },
              description: data.result.formatted_address
            };
            saveLocation(location);
            return location;
          } else {
            throw `request failed with status ${data.status} : ${data.error_message}`;
          }
        } catch (e) {
          throw e;
        }
      }
    }

    // // make it available from the vasco instance
    // vasco.places = { autocomplete, geocode };
    let app = vasco.server.app;

    // add api endpoints
    app.get("/api/autocomplete", async function(req, res) {
      if (req.query.input) {
        try {
          let results = await autocomplete(req.query.input);
          res.status(200).json(results);
        } catch (e) {
          logger.error(e);
          let msg = JSON.stringify(e);
          res.status(400).json({ msg });
        }
      } else {
        res.status(400).json({ msg: "Please provide a valid input" });
      }
    });
    app.get("/api/geocode", async function(req, res) {
      if (req.query.place_id) {
        try {
          let location = await geocode(req.query.place_id);
          res.status(200).json(location);
        } catch (e) {
          res.status(400).json({ msg: JSON.stringify(e) });
        }
      } else {
        res.status(400).json({ msg: "Please provide a valid place_id" });
      }
    });
  }
};

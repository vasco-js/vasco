const path = require("path");
const fs = require("fs-extra");
const _ = require("lodash");

const { knotToMs } = require("../utils");

module.exports = vasco => {
  const SpeedPolar = require("./speed-polar");
  const { Wind, Waterway } = vasco.$data;

  class Ship {
    #wind;
    #waterway;
    constructor(name, diagram, image) {
      this.name = name;
      this.speed = new SpeedPolar(diagram);
      this.img = image;

      this.#wind = Wind.createCentral();
      this.#waterway = Waterway.createCentral();

      this.save();
    }
    save() {
      fs.outputJsonSync(
        path.resolve(__dirname, "ships", `${this.name}.json`),
        this
      );
    }
    async getSpeed(lat, lon, angle, date) {
      let wind = await this.#wind.get(lat, lon, date);
      let dAngle = Math.abs(wind.angle - angle);
      let windSpeed = wind.speed;
      return knotToMs(this.speed.get(dAngle, windSpeed));
    }
    getSpeedSync(lat, lon, angle, date) {
      let wind = this.#wind.getSync(lat, lon, date);
      let dAngle = Math.abs(wind.angle - angle);
      let windSpeed = wind.speed;
      return knotToMs(this.speed.get(dAngle, windSpeed));
    }
    canNavigate(lat, lon) {
      let { isLand } = this.#waterway.get(lat, lon);
      return !isLand;
    }
    static load(name) {
      let { speed, img } = fs.readJsonSync(
        path.resolve(__dirname, "ships", `${name}.json`)
      );
      return new Ship(name, speed, img);
    }
  }

  // convert some
  function convertSome() {
    let images = {
      Amel64: "https://amel.fr/wp-content/uploads/2019/07/amel64-1024x697.jpg",
      Catamaran38:
        "https://www.sailmagazine.com/.image/t_share/MTUxMjExMzA3NTQwOTQ4MTYw/lead-sailing-shot-billy-black-photo-maine-cat-38-020817btsf-2264.jpg",
      Catamaran54:
        "https://newimages.yachtworld.com/resize/1/11/19/5791119_20160427100744509_1_XLARGE.jpg?f=/1/11/19/5791119_20160427100744509_1_XLARGE.jpg&w=924&h=693&t=1461780803000",
      Class40:
        "https://newimages.yachtworld.com/resize/1/2/22/6940222_20181221020552842_1_LARGE.jpg?f=/1/2/22/6940222_20181221020552842_1_LARGE.jpg&w=520&h=346&t=1545357994"
    };
    let catamarans = ["38", "54"].map(txt => `Catamaran${txt}.pol`);
    let candidates = ["Amel_64.pol", ...catamarans, "Class40.pol"];
    candidates.forEach(candidate => {
      let diagram = SpeedPolar.fromSource(candidate);
      let name = _.upperFirst(_.camelCase(candidate.split(".")[0]));
      let ship = new Ship(name, diagram, images[name]);
    });
  }
  async function test() {
    let test = Ship.load("Amel64");
    let date = new Date(Date.now() - 6 * 24 * 60 * 60 * 1000);
    let speed = await test.getSpeed(-4, 30, 110, date);
  }

  require("./copy")(vasco);
  vasco.Ship = Ship;
};

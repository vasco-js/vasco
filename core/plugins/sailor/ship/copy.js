const path = require("path");
const fs = require("fs-extra");

module.exports = function copy(vasco) {
  fs.copySync(
    path.resolve(__dirname, "ships"),
    path.resolve(vasco.getDir("build"), "content/client/ships")
  );

  let ships = fs
    .readdirSync(path.resolve(__dirname, "ships"))
    .map(file => file.replace(".json", ""));
  let index = `const ships = [ ${ships
    .map(name => `"${name}"`)
    .join(", ")} ];\n`;
  index += "export default ships";
  fs.outputFileSync(
    path.resolve(vasco.getDir("build"), "content/client/ships/index.js"),
    index
  );
};

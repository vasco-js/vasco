const fs = require("fs-extra");
const path = require("path");
const _ = require("lodash");

function fromTxt(name) {
  // gen a 2D array
  let str = fs
    .readFileSync(path.resolve(__dirname, "ressources/seapilot", name))
    .toString();
  let t = str.split("\n").map(line =>
    line
      .split("\t")
      .map(str => str.replace("\r", ""))
      .filter(str => str != "")
  );
  t = t.filter(line => line.length == t[0].length);

  // init diagram
  let diagram = {};
  diagram.windSpeeds = t[0].slice(1).map(parseFloat);
  diagram.angles = t
    .slice(1)
    .map(line => line[0])
    .map(parseFloat);

  // fill the diagram
  for (var j = 1; j <= diagram.windSpeeds.length; j++) {
    let windSpeed = t[0][j];
    for (var i = 1; i <= diagram.angles.length; i++) {
      let angle = t[i][0];
      let speed = parseFloat(t[i][j]);
      _.setWith(diagram, ["data", `${angle}`, windSpeed], speed, Object);
    }
  }

  return diagram;
}

exports.fromTxt = fromTxt;

const fs = require("fs-extra");
const path = require("path");
const _ = require("lodash");

const { fromTxt } = require("./convert");
const {
  normalizeAngle180,
  sandwich,
  isInBounds,
  interpolate
} = require("../../utils");

class SpeedPolar {
  constructor(diagram) {
    Object.assign(this, SpeedPolar.normalizeDiagram(diagram));
  }
  get(angle, windSpeed) {
    if (!isInBounds(this.windSpeeds, windSpeed)) {
      throw new Error("wind speed not in bounds");
    }
    angle = Math.abs(normalizeAngle180(angle));
    if (!isInBounds(this.angles, angle)) {
      throw new Error(`angle ${angle} not in bounds`);
    }
    return interpolate(
      angle,
      windSpeed,
      this.angles,
      this.windSpeeds,
      this.data
    );
  }
  static normalizeDiagram(diagram) {
    let angles = _.range(0, 180 + 1, 1);
    let windSpeeds = _.range(0, 60 + 5, 5);
    let data = {};

    // set values
    windSpeeds.forEach(windSpeed => {
      angles.forEach(angle => {
        let speed = interpolate(
          angle,
          windSpeed,
          diagram.angles,
          diagram.windSpeeds,
          diagram.data
        );
        _.setWith(data, [`${angle}`, `${windSpeed}`], speed, Object);
      });
    });

    return { angles, windSpeeds, data };
  }
  static fromSource(src) {
    return new SpeedPolar(fromTxt(src));
  }
}

module.exports = SpeedPolar;

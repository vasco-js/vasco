const _ = require("lodash");

const {
  sign,
  distance,
  latToAngle,
  angleToLat,
  lonToAngle,
  angleToLon
} = require("./utils");

class Location {
  constructor(opts) {
    Object.assign(this, opts);
  }
  toISO() {
    let absLat = Math.abs(this.lat);
    let absLon = Math.abs(this.lon);
    let lat = sign(this.lat) + (absLat < 10 ? "0" : "") + absLat;
    let lon =
      sign(this.lon) +
      (absLon < 100 ? "0" : "") +
      (absLon < 10 ? "0" : "") +
      absLon;
    return `${lat}${lon}/`;
  }
  distanceTo(loc) {
    return distance(this, loc);
  }
}

class EarthGrid {
  #cache;
  constructor(q) {
    this.q = q;
    this.d = 180 / q;
    this.nx = 2 * q;
    this.ny = q + 1;
    this.n = this.nx * this.ny;

    this.#cache = {
      locations: Array(this.n).fill(null),
      distances: Array(this.ny).map(x => {})
    };
  }
  getClosestLocation(lat, lon) {
    let x = this.closestX(lon),
      y = this.closestY(lat),
      loc = this.getLocation(x, y);
    return loc;
  }
  closestY(lat) {
    let d = this.d;
    let angle = latToAngle(lat);
    return Math.round(angle / d);
  }
  closestX(lon) {
    let d = this.d;
    let angle = lonToAngle(lon);
    return Math.round(angle / d);
  }
  yToLat(y) {
    let angle = y * this.d;
    return angleToLat(angle);
  }
  xToLon(x) {
    let angle = x * this.d;
    return angleToLon(angle);
  }
  getId(x, y) {
    return this.nx * y + x;
  }
  getXYFromId(k) {
    let { nx } = this;
    let y = ~~(k / nx);
    let x = k - y * nx;
    return [x, y];
  }
  forEach(f) {
    for (var y = 0; y < this.ny; y++) {
      for (var x = 0; x < this.nx; x++) {
        let loc = this.getLocation(x, y);
        f(loc);
      }
    }
  }
  getLocation(x, y) {
    let id = this.getId(x, y);
    let lon = this.xToLon(x);
    let lat = this.yToLat(y);

    let location = _.get(this.#cache, ["locations", id]);
    if (!location) {
      location = new Location({
        id,
        x,
        y,
        lat,
        lon
      });
      this.#cache.locations[id] = location;
    }

    return location;
  }
  getLocationById(id) {
    let [x, y] = this.getXYFromId(id);
    return this.getLocation(x, y);
  }
  getLocationFromDirection(base, direction) {
    let [offsetX, offsetY] = EarthGrid.parseDirection(direction);
    let { x, y } = base;
    let { id } = this.normalizeCoord(x + offsetX, y + offsetY);
    return this.getLocationById(id);
  }
  // DISTANCES
  distanceWithDirection(base, direction) {
    let { id: id1, x: x1, y: y1 } = base;
    let val = _.get(this.#cache, ["distance", y1, direction]);
    if (val == undefined) {
      let from = base;
      let to = this.getLocationFromDirection(from, direction);

      val = distance(from, to);
      _.set(this.#cache, ["distance", y1, direction], val);
    }
    return val;
  }
  // DIRECTIONS
  static genDirections(k) {
    let directions = ["n", "s", "e", "w"];
    let more = [["ne", "se", "sw", "nw"]];
    for (var i = 0; i < k; i++) {
      if (i > 0) {
        let newDirections = more[i - 1]
          .map(str => [str[0] + str, str + str[i]])
          .flat();
        more.push(newDirections);
        // generates following :
        // ["nee", "nne", "see","sse", "sww", "ssw", "nww", "nnw"]
        // etc ...
      }

      more[i].forEach(direction => directions.push(direction));
    }
    return directions;
  }
  static parseDirection(direction) {
    let x = 0;
    let y = 0;
    Array.from(direction).forEach(token => {
      switch (token) {
        case "n":
          y--;
          break;
        case "e":
          x++;
          break;
        case "s":
          y++;
          break;
        case "w":
          x--;
          break;
        default:
          throw `unknown direction token ${token}`;
      }
    });
    return [x, y];
  }
  // UTILS
  normalizeCoord(x, y) {
    if ([x, y].some(_.isNaN)) {
      throw new Error("coords must be numbers");
    }
    let { q, nx, ny } = this;

    // y
    if (y > ny) {
      y = y % (2 * q);
      return this.normalizeCoord(x + q, 2 * q - y);
    }
    if (y < 0) {
      return this.normalizeCoord(x + q, -y);
    }
    // x
    if (x >= 0) {
      x = x % nx;
    } else {
      return this.normalizeCoord(nx + x, y);
    }

    let id = this.getId(x, y);
    return { id, x, y };
  }
}
EarthGrid.Location = Location;

module.exports = EarthGrid;

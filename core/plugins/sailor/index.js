module.exports = {
  install(vasco) {
    vasco.EarthGrid = require("./earth-grid");
    require("./data")(vasco);
    require("./ship")(vasco);
  }
};

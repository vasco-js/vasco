const _ = require("lodash");

exports.sandwich = function sandwich(array, val) {
  let inf = Math.max(...array.filter(x => x <= val));
  let sup = Math.min(...array.filter(x => x >= val));
  return [inf, sup];
};
exports.isInBounds = function isInBounds(array, val) {
  return exports.sandwich(array, val).every(_.isFinite);
};

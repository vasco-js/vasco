exports.barycentre = function barycentre(val1, val2, cursor) {
  return (1 - cursor) * val1 + cursor * val2;
};

exports.sign = function sign(x) {
  return x < 0 ? "-" : "+";
};

exports.normalizeAngle180 = function normalizeAngle180(angle) {
  angle %= 360;
  if (angle > 180) angle = 360 - angle;
  if (angle <= -180) angle = 360 + angle;
  return angle;
};

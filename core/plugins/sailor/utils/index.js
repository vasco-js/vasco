const utils = {
  ...require("./maths"),
  ...require("./arrays"),
  ...require("./calculus"),
  ...require("./geo"),
  ...require("./sailing")
};

module.exports = utils;

const { sandwich } = require("./arrays");
const { barycentre } = require("./maths");

exports.interpolate = function interpolate(x, y, xArr, yArr, values) {
  // FIXME: Looks bad on the graph (might be a problem there)
  let [xInf, xSup] = sandwich(xArr, x);
  let [yInf, ySup] = sandwich(yArr, y);
  let dx = xSup - xInf;
  let dy = ySup - yInf;

  let yCursor = dy == 0 ? 0 : (y - yInf) / dy;
  let zInf = barycentre(values[xInf][yInf], values[xInf][ySup], yCursor),
    zSup = barycentre(values[xSup][yInf], values[xSup][ySup], yCursor);

  let xCursor = dx == 0 ? 0 : (x - xInf) / dx;
  let z = barycentre(zInf, zSup, xCursor);

  return z;
};

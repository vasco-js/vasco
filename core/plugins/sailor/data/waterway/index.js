const turf = require("@turf/turf");

const {
  sandwich,
  isInBounds,
  barycentre,
  latToAngle,
  lonToAngle
} = require("../../utils");

module.exports = vasco => {
  const EarthGrid = vasco.EarthGrid;

  class WaterwayCentral {
    constructor() {
      let feature = require("./ressources/lands.json");
      this.polygon = turf.multiPolygon(feature.geometry.coordinates);
      this.grid = new EarthGrid(1000);
    }
    get(lat, lon) {
      let location = this.grid.getClosestLocation(lat, lon);
      if (!location.waterway) {
        let point = turf.point([lon, lat]);
        let isLand = turf.booleanPointInPolygon(point, this.polygon);
        location.waterway = new Waterway(isLand);
      }
      return location.waterway;
    }
  }

  class Waterway {
    constructor(isLand) {
      this.isLand = isLand;
    }
    static createCentral() {
      return new WaterwayCentral();
    }
  }

  return Waterway;
};

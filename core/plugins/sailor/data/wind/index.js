const {
  sandwich,
  isInBounds,
  barycentre,
  latToAngle,
  lonToAngle
} = require("../../utils");

let Grib = require("grib2");

module.exports = vasco => {
  let { getGRIB } = require("./fetch")(vasco);
  class Wind {
    constructor(u, v) {
      // Speed : m.s-1
      this.speed = Math.sqrt(Math.pow(u, 2) + Math.pow(v, 2));
      // Angle : angle en géomètrie plane par rapport à l'axe des x dans le sens trigo
      this.angle = (Math.atan2(v, u) * 180) / Math.PI;
    }
    static createCentral() {
      return new WindCentral();
    }
  }

  class WindState {
    constructor(date) {
      this.date = date;
    }
    getU(lat, lon) {
      return this.grib.get(lat, lon).uComponentOfWind;
    }
    getV(lat, lon) {
      return this.grib.get(lat, lon).vComponentOfWind;
    }
    async load() {
      this.grib = new Grib({ strict: false });
      let { filePath } = await getGRIB(this.date);
      await this.grib.loadFile(filePath);
    }
  }

  class WindCentral {
    constructor() {
      this.cache = {
        states: {}
      };
    }
    async get(lat, lon, date) {
      let [dateInf, dateSup] = WindCentral.dateSandwich(date);
      await Promise.all([this.load(dateInf), this.load(dateSup)]);
      return getSync(lat, lon, date);
    }
    getSync(lat, lon, date) {
      let [dateInf, dateSup] = WindCentral.dateSandwich(date);
      let wind1 = this.getState(dateInf),
        wind2 = this.getState(dateSup);

      let cursor =
        (date.getTime() - dateInf.getTime()) /
        (dateSup.getTime() - dateInf.getTime());

      let u = barycentre(wind1.getU(lat, lon), wind2.getU(lat, lon), cursor);
      let v = barycentre(wind1.getV(lat, lon), wind2.getV(lat, lon), cursor);

      return new Wind(u, v);
    }
    getState(date) {
      let id = date.toISOString();
      if (!this.cache.states[id]) {
        let err = new Error(`wind at date ${date.toISOString()} not loaded`);
        err.code = "WINDNOTLOADED";
        // expose a way to resolve the error by calling an async function that tries to load the wind data
        err.loadWind = async function loadWind() {
          await this.load(date);
        }.bind(this);

        throw err;
      } else return this.cache.states[id];
    }
    async load(date) {
      let id = date.toISOString();
      let state = this.cache.states[id];
      if (!state) {
        state = new WindState(date);
        await state.load();
        this.cache.states[id] = state;
      }
      return state;
    }
    static dateSandwich(date) {
      let hourInf = 6 * ~~(date.getUTCHours() / 6);
      let dateInf = new Date(date.getTime());
      dateInf.setUTCMilliseconds(0);
      dateInf.setUTCSeconds(0);
      dateInf.setUTCMinutes(0);
      dateInf.setUTCHours(hourInf);
      let dateSup = new Date(dateInf.getTime());
      dateSup.setUTCHours(hourInf + 6);
      return [dateInf, dateSup];
    }
  }

  // require("./auto-update")(vasco)

  return Wind;
};

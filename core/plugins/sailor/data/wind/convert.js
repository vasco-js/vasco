const fs = require("fs-extra");
const path = require("path");
const grib2json = require("grib2json").default;

module.exports = function convert(src) {
  return new Promise(function(resolve, reject) {
    try {
      grib2json(
        src,
        {
          scriptPath: path.resolve(
            __dirname,
            "../../lib/grib2json/bin/grib2json"
          ),
          names: true, // (default false): Return descriptive names too
          data: true // (default false): Return data, not just headers
        },
        function(err, json) {
          if (err) return console.error(err);
          resolve(json);
        }
      );
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = function(vasco) {
  let { getGRIB } = require("./fetch")(vasco);
  function updateWind() {
    let HOUR = 60 * 60 * 1000;
    let DAY = 24 * HOUR;
    let today = new Date();
    today.setUTCHours(0);
    today.setUTCMinutes(0);
    today.setUTCSeconds(0);
    today.setUTCMilliseconds(0);

    let promises = [];
    // today
    for (var k = 0; k <= new Date().getUTCHours() / 6 - 1; k++) {
      let date = new Date(today.getTime() + 6 * k * HOUR);
      promises.push(getGRIB(date));
    }
    // past days
    for (var i = 1; i < 3; i++) {
      for (var k = 0; k < 4; k++) {
        let date = new Date(today.getTime() - i * DAY + 6 * k * HOUR);
        promises.push(getGRIB(date));
      }
    }
    Promise.all(promises).then(() => {
      vasco.logger.log("verbose", "wind data up to date");
    });
  }
  updateWind();
};

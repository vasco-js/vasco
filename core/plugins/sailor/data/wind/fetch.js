const fs = require("fs-extra");
const path = require("path");
const axios = require("axios");
const fetch = require("node-fetch");

const convert = require("./convert");

module.exports = vasco => {
  const logger = vasco.logger;
  const WIND_DIR = path.resolve(vasco.getDir("build"), "./cache/weather/wind");

  async function getGRIB(date) {
    let id = date.toISOString().slice(0, 13);
    let grib2Path = path.resolve(WIND_DIR, `${id}.grib2`);
    if (!fs.pathExistsSync(grib2Path)) {
      let day = id.replace(/\-/g, "").slice(0, 8);
      let hour = id.slice(11, 13);
      // pick here https://nomads.ncep.noaa.gov/
      // date is YYYYMMDD
      let url = `https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_sflux.pl?file=gfs.t${hour}z.sfluxgrbf000.grib2&lev_10_m_above_ground=on&var_UGRD=on&var_VGRD=on&leftlon=0&rightlon=360&toplat=90&bottomlat=-90&dir=%2Fgfs.${day}%2F${hour}`;
      logger.log("verbose", `Wind [${id}] : requiring grib...`);
      // get GRIB2
      let res = await fetch(url);
      if (res.status != 200)
        throw new Error(`can't fetch grib data (${id}) : ${res.statusText}`);
      logger.log("verbose", `Wind [${id}] : downloading file...`);
      let buffer = await res.buffer();
      await fs.outputFile(grib2Path, buffer);
      logger.log("verbose", `Wind [${id}] : saved grib file !`);
    }
    return { filePath: grib2Path };
  }

  async function getJson(date) {
    let id = date.toISOString().slice(0, 13);
    let jsonPath = path.resolve(WIND_DIR, `${id}.json`);
    if (fs.pathExistsSync(jsonPath)) {
      return fs.readJsonSync(jsonPath);
    } else {
      await getGRIB(date);
      logger.log("verbose", `Wind [${id}] : converting...`);
      let json = await convert(grib2Path);
      await fs.outputJson(jsonPath, json);
      logger.log("verbose", `Wind [${id}] : saved json file !`);
      return json;
    }
  }
  return { getJson, getGRIB };
};

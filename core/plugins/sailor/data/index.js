module.exports = function registerMethods(vasco) {
  const Wind = require("./wind")(vasco);
  const Waterway = require("./waterway")(vasco);

  vasco.$data = {
    Wind,
    Waterway
  };
};

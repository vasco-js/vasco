module.exports = vasco => ({
  command: "reload",
  desc: "Reload things",
  handler: () => {
    vasco.reload();
  }
});

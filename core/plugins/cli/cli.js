const fs = require("fs-extra");
const path = require("path");

const yargs = require("yargs").scriptName("vasco");

class Cli {
  #vasco;
  constructor(vasco) {
    this.#vasco = vasco;
    this.commands = {};

    // add any command found in the commands directory
    let cmdDir = path.join(__dirname, "./commands");
    fs.readdirSync(cmdDir)
      .map(name => require(path.join(cmdDir, name)))
      .forEach(this.addCommand, this);

    // listener
    function listener(data) {
      let str = data.toString().trim();
      let args = str.split(" ");
      yargs.parse(args);
    }
    function removeListener() {
      process.stdin.removeListener("data", listener);
    }
    vasco.$once("reloading", removeListener);

    // listen to stdin
    process.stdin.on("data", listener);
  }
  addCommand(builder) {
    let command = builder(this.#vasco);
    this.commands[command.command] = command;
    yargs.command(command);
  }
}

module.exports = Cli;

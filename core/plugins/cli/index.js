const Cli = require("./cli");

module.exports = {
  install(vasco) {
    let logger = vasco.logger;

    vasco.$cli = new Cli(vasco);
  }
};

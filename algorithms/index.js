const path = require("path");
const fs = require("fs-extra");
const { Worker } = require("worker_threads");
const { EventEmitter2 } = require("eventemitter2");

const buildAlgorithm = require("./build-algorithm");

class VascoAlgorithmManager {
  #vasco;
  #events;
  constructor(vasco) {
    this.#vasco = vasco;
    this.#events = new EventEmitter2({ wildcard: true });
  }
  loadFromDir(dir) {
    let sources = fs
      .readdirSync(dir)
      .map(name => path.join(dir, name))
      .filter(source => fs.lstatSync(source).isDirectory());
    this.register(...sources);
  }
  register(...algorithms) {
    let logger = this.#vasco.logger;
    algorithms.forEach(
      function(src) {
        try {
          let options = require(src);
          let algorithm = buildAlgorithm(this.#vasco, {
            ...options,
            path: src
          });
          let name = algorithm.$name;
          this[name] = algorithm;
        } catch (e) {
          logger.error(e.stack || e);
          throw `Can't load algorithm at path ${src}`;
        }
      }.bind(this)
    );
  }
  // register(...algorithms) {
  //   algorithms.forEach(
  //     function(options) {
  //       let algorithm = buildAlgorithm(this.#vasco, options);
  //       let name = algorithm.$name;
  //       this[name] = algorithm;
  //     }.bind(this)
  //   );
  // }
  array() {
    return Object.values(this);
  }
  get(name) {
    if (this.hasOwnProperty(name)) {
      return this[name];
    } else {
      throw `Algorithm ${name} not found`;
    }
  }
  abortAll() {
    this.#events.emit("abortall");
  }
  async run(name, props, socket) {
    let logger = this.#vasco.logger;
    // multithreading
    let Algorithm = this.get(name);
    let id = `${Algorithm.$name}-${Algorithm.$created++}`;
    let debug = Algorithm.$debug;

    let vascoConfig = this.#vasco.config.getProperties();
    let workerData = {
      vascoConfig,
      name,
      props,
      id,
      debug
    };
    let worker = new Worker(require.resolve("./run"), {
      workerData,
      stdout: true,
      stderr: true
    });
    let aborted = false;

    worker.stdout.on("data", function(data) {
      logger.log("verbose", `[${id}] ${data.toString().trim()}`);
    });
    worker.stderr.on("data", function(data) {
      logger.log("error", `[${id}] ${data.toString().trim()}`);
    });

    worker.on("message", message => {
      try {
        if (debug) {
          logger.log("debug", `[${id}] ${message.event}`);
        }
        if (socket) {
          socket.emit(`${name}:${message.event}`, message.payload);
        }
      } catch (e) {
        throw new Error(`Error treating message : ${message}`);
      }
    });
    worker.on("error", logger.error);
    worker.on("exit", code => {
      logger.log("verbose", `[${id}] exited with code ${code}`);
      if (code !== 0 && !aborted)
        logger.error(new Error(`Worker stopped with exit code ${code}`));
    });

    function abort() {
      logger.log("verbose", `[${id}] abort`);
      if (socket) {
        socket.emit(`${name}:exit`, { status: "aborted" });
      }
      aborted = true;
      worker.terminate();
    }

    this.#events.on("abortall", abort);
  }
}

module.exports = VascoAlgorithmManager;

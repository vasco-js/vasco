const { workerData, parentPort } = require("worker_threads");

let { vascoConfig, name, props, id, debug } = workerData;

const Vasco = require("../index.js");
let vasco = new Vasco(vascoConfig, { id, debug });
vasco.load();

try {
  let Algorithm = vasco.algorithms.get(name);
  let algorithm = new Algorithm();
  algorithm.$onAny(function(event, payload) {
    parentPort.postMessage({ event, payload });
  });
  algorithm.run(props);
} catch (e) {
  console.error(e);
}

// You can do any heavy stuff here, in a synchronous way
// without blocking the "main thread"
// parentPort.postMessage({ hello: "hi" });

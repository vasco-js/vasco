const { EventEmitter2 } = require("eventemitter2");
const _ = require("lodash");
const convict = require("convict");

function validate_options(options) {
  let name = _.get(options, "name");
  if (name == undefined) {
    throw `AlgorithmError: [???] please provide a name to your algorithms`;
  }

  ["run", "error", "success"].forEach(funcName => {
    let func = _.get(options, funcName);
    if (func == undefined) {
      throw new Error(
        `AlgorithmError: [${name}] please provide a ${funcName} function`
      );
    } else if (typeof func != "function") {
      throw new Error(
        `AlgorithmError: [${name}] ${funcName} must be a function`
      );
    }
  });
}

module.exports = function(vasco, opts) {
  validate_options(opts);

  let { path, name, debug, methods, run, error, success, progression } = opts;

  class Algorithm {
    #events;
    #event_cache;
    constructor(id) {
      this.vasco = vasco;
      this.#events = new EventEmitter2();
      this.#event_cache = {};

      this.config = convict(opts.props);

      this.id = id;
    }
    run(props) {
      try {
        this.inject_props(props);
        this.$emit("run", this.config.getProperties());
      } catch (e) {
        this.$emit("run", props);
        this.error(`PropsError : ${e}`);
        return;
      }

      // only if no errors
      run.bind(this)(props);
    }
    async error(...args) {
      let output = await error.bind(this)(...args);
      this.$emit("exit", { status: "error", output });
    }
    async success(...args) {
      let output = await success.bind(this)(...args);
      this.$emit("exit", { status: "success", output });
    }
    $progression(...args) {
      // debounce
      let last_date = _.get(this.#event_cache, "progression.last_date");
      let now = Date.now();
      if (last_date && now - last_date < 500) {
        return;
      }
      _.set(this.#event_cache, "progression.last_date", now);

      let output;
      if (progression) {
        output = progression.bind(this)(...args);
      }
      this.$emit("progression", { output });
    }
    $on(e, handler) {
      this.#events.on(e, handler);
    }
    $onAny(handler) {
      this.#events.onAny(handler);
    }
    $emit(e, payload) {
      this.#events.emit(e, payload);
    }
    inject_props(props) {
      if (props) this.config.load(props);
    }
  }
  // inject methods
  Object.entries(methods).forEach(([name, method]) => {
    Algorithm.prototype[name] = method;
  });

  Algorithm.$name = name;
  Algorithm.$path = path;
  Algorithm.$debug = debug;

  Algorithm.$created = 0;

  return Algorithm;
};

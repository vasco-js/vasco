const fs = require("fs-extra");
const path = require("path");
const _ = require("lodash");
const chokidar = require("chokidar");

const uuidv1 = require("uuid/v1");

const { EventEmitter2 } = require("eventemitter2");

const createLogger = require("./logger");

const VascoServer = require("@vasco/server");
const VascoClient = require("@vasco/client");

const VascoPluginManager = require("./plugins");
const VascoAlgorithmManager = require("./algorithms");

const convict = require("convict");
const config_schema = require("./config");
const defaults = {}; //require("./defaults");

class Vasco {
  #events;
  constructor(config, worker = false) {
    this.id = uuidv1();

    this.config = convict(config_schema);
    this.config.load(config);
    this.config.validate();

    this.#events = new EventEmitter2();

    // logger
    if (worker) {
      let { id, debug } = worker;
      this.logger = createLogger(debug, ({ format }) => ({
        format: format.combine(
          format.cli(),
          format(({ message }) => {
            return `[${id}] ` + message;
          })()
        )
      }));
    } else {
      let debug = this.isDev;
      this.logger = createLogger(debug);
    }

    this.server = new VascoServer(this);
    this.client = new VascoClient(this);
  }
  // events
  $on(e, handler) {
    this.#events.on(e, handler);
  }
  $once(e, handler) {
    this.#events.once(e, handler);
  }
  $onAny(handler) {
    this.#events.onAny(handler);
  }
  $emit(e, payload) {
    this.#events.emit(e, payload);
  }
  load() {
    // install plugins
    this.logger.log("verbose", "installing plugins ...");
    this.plugins = new VascoPluginManager(this);
    this.plugins.loadFromDir(this.getDir("plugins"));

    // register algorithms
    this.logger.log("verbose", "registering algorithms ...");
    this.algorithms = new VascoAlgorithmManager(this);
    this.algorithms.loadFromDir(this.getDir("algorithms"));
  }
  reload() {
    this.logger.info("\nreloading ...");
    this.$emit("reloading");
    for (module in require.cache) {
      delete require.cache[module];
    }
    this.algorithms.abortAll();

    this.server.stop();
    this.server = new VascoServer(this);

    this.load();
    this.server.init();
    this.server.start();
  }
  start() {
    let logger = this.logger;

    this.load();

    if (!this.config.get("client.disable")) this.client.init();
    this.server.init();

    let vasco = this;
    if (this.isDev) {
      // watch file changes
      let ignored = [
        "**/node_modules/**",
        "**/" + path.relative(this.getDir("root"), this.getDir("build")) + "/**"
      ];
      chokidar
        .watch([this.getDir("root"), "."], {
          ignored,
          ignoreInitial: true
        })
        .on("all", (event, path) => {
          logger.log("verbose", `${event} ${path}`);
          vasco.reload();
        });
    }

    logger.info("starting ...");
    if (!this.config.get("client.disable")) this.client.start();
    this.server.start();
  }
  get isDev() {
    return this.config.get("dev");
  }
  getDir(name) {
    let rootDir = this.config.get("rootDir");
    switch (name) {
      case "root":
        return rootDir;
        break;
      default:
        return path.join(rootDir, this.config.get("dir")[name]);
    }
  }
}

module.exports = Vasco;
